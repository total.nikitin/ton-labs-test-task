"""
title           :tests_longest_substring.py
description     :tests covering longest_substring function
python_version  :3.6
"""

from longest_substring import  longest_substring

def send_msg(channel, msg):
    print("message --channel \"{}\" \"{}\"".format(channel, msg))


def success():
    print("succeed")


def fail():
    print("failed")


def test_case(input, expected):
    actual = longest_substring(input)
    assert actual == expected, "Running longest_substring('{0}')... Expected {1}, got {2}".format(
        input, expected, actual)


def test_longest_substring():
    try:
        test_case(None, None)
        test_case("", "")
        test_case("a", "a")
        test_case("abc", "ab")
        test_case("abcc", "bcc")
        test_case("aabbcc", "aabb")
        test_case("ababcc", "abab")
        test_case("aaabbcc", "aaabb")
        test_case("aaaabbccccc", "bbccccc")
        success()
    except AssertionError as e:
        fail()
        print(e)


if __name__ == "__main__":
    test_longest_substring()