"""
title           :longest_substring.py
description     :function which returns longest substring with at most 2 distinct characters
python_version  :3.6
"""


def longest_substring(input_string, k = 2):
    if input_string is None:
        return input_string

    start = 0
    result = ""
    freq = {}
    i = 0

    while i < len(input_string):
        c = input_string[i]
        if c in freq:
            freq[c] += 1
        else:
            freq[c] = 1

        if len(freq) == k + 1:
            if i - start > len(result):
                result = input_string[start:i]
            while len(freq) > k:
                c = input_string[start]
                if freq[c] == 1:
                    freq.pop(c, None)
                else:
                    freq[c] -= 1
                start += 1
        i += 1

    if len(freq) <= k and len(input_string) - start > len(result):
        return input_string[start:]

    return result
